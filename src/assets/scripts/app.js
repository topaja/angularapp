// MODULE
var myApp = angular.module('myApp', ['ngRoute','ngAnimate', 'ngTouch','angularUtils.directives.dirPagination']);

// ROUTES
myApp.config(function ($routeProvider) {
   
    $routeProvider
    
    .when('/', {
        templateUrl: 'pages/home.html',
        controller: 'homeController'
    })
    
    .when('/first', {
        templateUrl: 'pages/first.html',
        controller: 'firstController'
    })

    .when('/second', {
        templateUrl: 'pages/second.html',
        controller: 'secondController'
    })

    .when('/third', {
        templateUrl: 'pages/third.html',
        controller: 'thirdController'
    })


    .when('/fourth', {
        templateUrl: 'pages/fourth.html',
        controller: 'fourthController'
    })
    
});

// CONTROLLERS
//myApp.controller('homeController', ['$scope', function($scope) {
    
//}]);

// tutaj takze moglby byc ten kontroler, ale zostal przeniesiony do first.js
// myApp.controller('firstController', ['$scope', function($scope) {
// }]);

